const mysql = require("mysql");
const CREDENTIALS = require("../config/mysql");

function connection() {
  const connection = mysql.createConnection(CREDENTIALS);
  return connection;
}

function registerUser(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_USER} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}
/* Registrar citas */
function registerAppointment(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_APPOINTMENT} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function updateAppointment(data) {

  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let id = data.query.id;
    let insert = `UPDATE ${process.env.TABLE_APPOINTMENT} SET ? WHERE id = ?`;
    let query = mysqlConnection.format(insert, [data.body, id]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function updateUser(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let id = data.id;
    let insert = `UPDATE ${process.env.TABLE_USER} SET ? WHERE id = ?`;
    let query = mysqlConnection.format(insert, [data.req.body, id]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

/*Register pet*/

function registerPet(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_PET} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

/*Consult Pet*/
function consultPets(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let user_id = data.user_id;
    let select = `SELECT p.id, p.name, p.race, p.weight, date_format(p.birthdate, "%Y-%m-%d") AS birthdate, p.type_pet, p.user_id FROM ${process.env.TABLE_PET} p WHERE user_id=?`;
    let query = mysqlConnection.format(select, [user_id]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function deletePet(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let id = data.id;
    let user_id = data.user_id;
    let deleteUser = `DELETE FROM ${process.env.TABLE_PET} WHERE id=? and user_id=?`;
    let query = mysqlConnection.format(deleteUser, [id, user_id]);
    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve("Pet deleted successfully");
    });
  });
}

/*Consult User*/
function consultUser(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let id = data.id;
    let select = `SELECT * FROM ${process.env.TABLE_USER} WHERE id=?`;
    let query = mysqlConnection.format(select, [id]);
    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function deleteUser(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let id = data.id;
    let deleteUser = `DELETE FROM ${process.env.TABLE_USER} WHERE id=?`;
    let query = mysqlConnection.format(deleteUser, [id]);
    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve("User deleted successfully");
    });
  });
}

function login(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let email = data.email;
    let select = `SELECT id, email, password FROM ${process.env.TABLE_USER} WHERE email=?`;
    let query = mysqlConnection.format(select, [email]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function updatePet(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let id = data.req.query.id;
    let user_id = data.user_id;
    let update = `UPDATE ${process.env.TABLE_PET} SET ? WHERE id = ? and user_id = ?`;
    let query = mysqlConnection.format(update, [data.req.body, id, user_id]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}
function registerMedicalHistory(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let insert = `INSERT INTO ${process.env.TABLE_MEDICAL_HISTORY} SET ?`;
    let query = mysqlConnection.format(insert, data);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}

function updateMedicalHistory(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let id = data.id;
    let update = `UPDATE ${process.env.TABLE_MEDICAL_HISTORY} SET ? WHERE id = ?`;
    let query = mysqlConnection.format(update, [data, id]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}
function consultMedicalAppointment(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let select = `SELECT ma.id, date_format(ma.date, "%Y-%m-%d") AS fecha_cita,ma.hour, ma.state,p.name FROM ${process.env.TABLE_APPOINTMENT} ma inner join ${process.env.TABLE_PET} p ON ma.pet_id = p.id WHERE ma.pet_id = ?`;
    
    let id = data.pet_id;
    let query = mysqlConnection.format(select, [id]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function consultMedicalHistory(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let select = `SELECT mh.id,mh.name_vet, mh.description, date_format(mh.date, "%Y-%m-%d") AS date,p.name FROM ${process.env.TABLE_MEDICAL_HISTORY} mh inner join ${process.env.TABLE_PET} p ON mh.pet_id = p.id WHERE p.user_id = ?`;
    
    let id = data.user_id;
    let query = mysqlConnection.format(select, [id]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

function updateMedicalHistory(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });

    let id = data.id;
    let update = `UPDATE ${process.env.TABLE_MEDICAL_HISTORY} SET ? WHERE id = ?`;
    let query = mysqlConnection.format(update, [data, id]);

    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();

      resolve(result);
    });
  });
}
function consultMedicalAppointment(data) {
  return new Promise((resolve, reject) => {
    const mysqlConnection = connection();
    mysqlConnection.connect((err) => {
      if (err) throw err;
      console.log("Connected to MySQL Server!");
    });
    let select = `SELECT ma.id, date_format(ma.date, "%Y-%m-%d") AS date,ma.hour, ma.state,p.name, ma.pet_id FROM ${process.env.TABLE_APPOINTMENT} ma inner join ${process.env.TABLE_PET} p ON ma.pet_id = p.id WHERE p.user_id = ?`;
    let id = data.user_id;
    let query = mysqlConnection.format(select, [id]);
    mysqlConnection.query(query, (error, result) => {
      if (error) reject(error);
      mysqlConnection.end();
      resolve(result);
    });
  });
}

module.exports = {
  connection,
  registerUser,
  registerPet,
  consultPets,
  consultUser,
  deleteUser,
  registerAppointment,
  updateAppointment,
  deletePet,
  updateUser,
  login,
  updatePet,
  registerMedicalHistory,
  updateMedicalHistory,
  consultMedicalAppointment,
  consultMedicalHistory,
};
