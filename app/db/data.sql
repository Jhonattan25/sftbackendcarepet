/* ----- CREAR BASE DE DATOS ----- */
create schema carepetproject;
use carepetproject;

CREATE TABLE user(
    id VARCHAR(15) PRIMARY KEY NOT NULL,
    name      VARCHAR (50) NOT NULL ,
    lastname  VARCHAR (50) NOT NULL ,
    address   VARCHAR (100) NOT NULL ,
    email     VARCHAR (100) NOT NULL,
    cellphone VARCHAR (15) NOT NULL ,
    type_role VARCHAR(15) NOT NULL ,
    password VARCHAR (500) NOT NULL
  ) ;
  
CREATE TABLE pet(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    race VARCHAR(30) NOT NULL,
    weight DOUBLE NOT NULL,
    birthdate DATE NOT NULL,
    user_id VARCHAR(15) NOT NULL,
    type_pet VARCHAR(30) NOT NULL,
    FOREIGN KEY Pet_user_FK (user_id) REFERENCES user(id)
  ) ;

  CREATE TABLE sales_invoice
  (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    date DATE NOT NULL ,
    total_pay DOUBLE NOT NULL ,
    user_id VARCHAR(15) NOT NULL,
	FOREIGN KEY Sales_invoice_user_FK (user_id) REFERENCES user(id)
  ) ;
  
  CREATE TABLE product(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL ,
    description VARCHAR (1000) NOT NULL ,
    amount INT NOT NULL,
    url_image VARCHAR(500),
    price DOUBLE NOT NULL
  ) ;
  
  CREATE TABLE sale_detail(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    amount INT NOT NULL ,
	unit_price DOUBLE NOT NULL ,
    sales_invoice_id INT NOT NULL,
    product_id INT NOT NULL,
    FOREIGN KEY Sale_detail_Sales_invoice_FK (sales_invoice_id) REFERENCES sales_invoice(id),
    FOREIGN KEY Sale_detail_Product_FK (product_id) REFERENCES product(id)
  ) ;

CREATE TABLE medical_appointment(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    date DATE NOT NULL ,
    hour CHAR(5) NOT NULL ,
    state  VARCHAR (20) NOT NULL ,
    pet_id INT NOT NULL,
	FOREIGN KEY Medical_appointment_Pet_FK (pet_id) REFERENCES pet(id)
  ) ;

CREATE TABLE medical_history(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name_vet VARCHAR(100) NOT NULL,
    description VARCHAR (1000) NOT NULL,
    date DATE NOT NULL,
    pet_id INT NOT NULL,
    FOREIGN KEY Medical_history_Pet_FK (pet_id) REFERENCES pet(id)
  ) ;