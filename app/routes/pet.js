const express = require('express');
const authToken = require('../middleware/auth-token');
const petController = require('../controllers/pet-controller');
const registerPetValidator = require('../middleware/registerPet-validator');
const deletePetValidator = require('../middleware/deletePet-validator');
const router = express.Router();

router.post('/register', authToken.njwtAuth, registerPetValidator.validatorParams, registerPetValidator.validator, petController.registerPet);
router.get('/consult', authToken.njwtAuth, petController.consultPets);
router.put('/update', authToken.njwtAuth, registerPetValidator.validatorParams, registerPetValidator.validator, petController.updatePet);
router.delete('/delete', authToken.njwtAuth, deletePetValidator.validatorParams, deletePetValidator.validator, petController.deletePet);

module.exports = router;