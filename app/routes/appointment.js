const express = require('express');
const registerAppointmentValidator = require('../middleware/registerAppointment-validator');
const registerAppointmentController = require('../controllers/medicalAppointment-controller');
const authToken = require('../middleware/auth-token');
const router = express.Router();



router.post('/register', registerAppointmentValidator.validatorParams, registerAppointmentValidator.validator, registerAppointmentController.registerAppointment);
router.put('/update', registerAppointmentValidator.validatorParams, registerAppointmentValidator.validator, registerAppointmentController.updateAppointment);
//router.put('/cancel', authToken.njwtAuth, registerAppointmentController.cancelAppointment);
router.get('/consult',authToken.njwtAuth, registerAppointmentController.consultMedicalAppointment);


module.exports = router;
