const express = require('express');
const authToken = require('../middleware/auth-token');
const medicalHistoryController = require('../controllers/medicalHistory-controller');
const medicalHistoryValidator = require('../middleware/registerMedicalHistory-validator');
const modifyMedicalHistoryValidator = require('../middleware/modifyMedicalHistory-validator');
const router = express.Router();

router.post('/register', authToken.njwtAuth, medicalHistoryValidator.validatorParams, medicalHistoryValidator.validator, medicalHistoryController.registerMedicalHistory);
router.put('/update', authToken.njwtAuth, modifyMedicalHistoryValidator.validatorParams, modifyMedicalHistoryValidator.validator, medicalHistoryController.updateMedicalHistory);
router.get('/consult', authToken.njwtAuth, medicalHistoryController.consultMedicalHistory);

module.exports = router;