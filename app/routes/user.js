const express = require('express');
const authToken = require('../middleware/auth-token');
const userController = require('../controllers/user-controller');
const registerUserValidator = require('../middleware/registerUser-validator');
const updateUserValidator = require('../middleware/updateUser-validator');
const router = express.Router();

router.post('/register', registerUserValidator.validatorParams, registerUserValidator.validator, userController.register);
router.get('/consult', authToken.njwtAuth, userController.consultUser);
router.put('/update', updateUserValidator.validatorParams, updateUserValidator.validator, userController.updateUser);
router.delete('/delete', authToken.njwtAuth, userController.deleteUser);

module.exports = router;