require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const bearerToken = require('express-bearer-token');
const cookieParser = require('cookie-parser');

const user = require('./routes/user');
const appointment = require('./routes/appointment');
const pet = require('./routes/pet');
const login = require('./routes/login');
const medicalHistory = require('./routes/medicalHistory');
const medicalAppointment = require('./routes/appointment');

const app = express()
  .use(cors({ credentials: true, origin: "http://localhost:4200" }))
  .use(bodyParser.urlencoded({ extended: false }))
  .use(bodyParser.json())
  .use(cookieParser())
  .use(bearerToken());

app.use('/user', user);
app.use('/appointment',appointment);
app.use('/pet', pet);
app.use('/login', login);
app.use('/medicalHistory', medicalHistory);
app.use('/medicalAppointment', medicalAppointment);

module.exports = app;