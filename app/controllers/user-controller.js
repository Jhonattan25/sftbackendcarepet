const db = require("../db/mysql");
const bcrypt = require("bcryptjs");
const nJwt = require("njwt");
const config = require("../config/keys");

let register = (req, res) => {
  let hashPass = bcrypt.hashSync(req.body.password, 8);
  req.body.password = hashPass;
  db.registerUser(req.body)
    .then(async (result) => {
      return res.status(200).json({
        status: "Successful registration",
        reg: true,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let consultUser = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.consultUser({ id: decoded.body.id })
      .then((result) => {
        return res.status(200).json({
          status: "OK",
          data: result,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let updateUser = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.updateUser({ req, id: decoded.body.id })
      .then((result) => {
        if (result.changedRows == 1) {
          return res.status(200).json({
            status: "200",
            data: "User updated successfully",
          });
        }
        return res.status(400).json({
          status: "400",
          data: "The user could not be updated",
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let deleteUser = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.deleteUser({ id: decoded.body.id })
      .then((result) => {
        return res.status(200).json({
          status: "OK",
          info: result,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

module.exports = {
  register,
  consultUser,
  updateUser,
  deleteUser,
};
