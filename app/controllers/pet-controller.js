const db = require("../db/mysql");
const nJwt = require("njwt");
const config = require("../config/keys");

let registerPet = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    req.body["user_id"] = decoded.body.id;

    db.registerPet(req.body)
      .then((result) => {
        return res.status(200).json({
          status: "added pet successfully",
          add: true,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let consultPets = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.consultPets({ user_id: decoded.body.id })
      .then((result) => {
        return res.status(200).json({
          status: "Ok",
          pets: result,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let updatePet = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.updatePet({ req, user_id: decoded.body.id })
      .then((result) => {
        if (result.changedRows == 1) {
          return res.status(200).json({
            status: "200",
            data: "Pet updated successfully",
          });
        }
        return res.status(400).json({
          status: "400",
          data: "The Pet could not be updated",
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let deletePet = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    if (err) {
      return res.status(500).send({
        status: "Authentication failed",
        auth: false,
        message: err,
      });
    }
    db.deletePet({ id: req.query.id, user_id: decoded.body.id })
      .then((result) => {
        return res.status(200).json({
          status: "OK",
          info: result,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

module.exports = {
  registerPet,
  consultPets,
  updatePet,
  deletePet,
};
