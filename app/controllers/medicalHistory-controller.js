const db = require("../db/mysql");
const nJwt = require("njwt");
const config = require("../config/keys");

let registerMedicalHistory = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    //req.body["user_id"] = decoded.body.id;

    db.registerMedicalHistory(req.body)
      .then((result) => {
        return res.status(200).json({
          status: "added medical history successfully",
          add: true,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

let updateMedicalHistory = (req, res) => {
  db.updateMedicalHistory(req.body)
    .then((result) => {
      return res.status(200).json({
        status: "Modified medical history successfully",
        mod: true,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let consultMedicalHistory = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
  req.body["user_id"] = decoded.body.id;

    db.consultMedicalHistory(req.body)
      .then((result) => {
        return res.status(200).json({
          status: "consult medical history successfully",
          medicalHistory: result,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

module.exports = {
  registerMedicalHistory,
  updateMedicalHistory,
  consultMedicalHistory,
};
