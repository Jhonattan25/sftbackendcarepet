const db = require("../db/mysql");
const nJwt = require("njwt");
const config = require("../config/keys");

let registerAppointment = (req, res) => {
  db.registerAppointment(req.body)
    .then((result) => {
      return res.status(200).json({
        status: "added appointment successfully",
        add: true,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

let updateAppointment = (req, res) => {
    db.updateAppointment(req)
    .then((result) => {
      if (result.changedRows == 1) {
        return res.status(200).json({
          status: "200",
          data: "Appointment updated successfully",
        });
      }
      return res.status(400).json({
        status: "400",
        data: "The Appointment could not be updated",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};


let consultMedicalAppointment = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    req.body["user_id"] = decoded.body.id;
    db.consultMedicalAppointment(req.body)
      .then((result) => {
        return res.status(200).json({
          status: "Ok",
          medical: result,
        });
      })
      .catch((err) => {
        console.log(err);
      });
});
};


let cancelAppointment = (req, res) => {
  let sub = req.header("Authorization").split(" ");
  let token = sub[1];
  nJwt.verify(token, config.SIGNING_KEY, function (err, decoded) {
    req.body["user_id"] = decoded.body.id;
    db.cancelAppointment(req.body)
    .then((result) => {
      return res.status(200).json({
        status: "cancel appointment successfully",
        add: true
      });
    })
    .catch((err) => {
      console.log(err);
    });
  });
  };

module.exports = {
  registerAppointment,
  updateAppointment,
  consultMedicalAppointment,
  cancelAppointment,
};
