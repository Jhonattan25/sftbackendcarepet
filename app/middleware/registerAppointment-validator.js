const { check, validationResult } = require("express-validator");

validatorParams = [
  check("date").isDate(),
  check("hour").isLength({ min: 1}),
  check("state").isLength({ min: 1 }),
  check("pet_id").isNumeric(),
];

function validator(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  next();
}


module.exports = {
  validatorParams,
  validator,
};