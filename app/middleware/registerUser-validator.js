const { check, validationResult } = require("express-validator");

validatorParams = [
  check("id").isLength({ min: 8, max: 10 }),
  check("name").isLength({ min: 1 }),
  check("lastname").isLength({ min: 1 }),
  check("address").isLength({ min: 1 }),
  check("email").isEmail(),
  check("cellphone").isLength({ min: 10, max: 10 }),
  check("type_role").isLength({ min: 1 }),
  check("password").isLength({ min: 8, max: 15 }),
];

function validator(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  next();
}

module.exports = {
  validatorParams,
  validator,
};