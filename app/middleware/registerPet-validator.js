const { check, validationResult } = require("express-validator");

validatorParams = [
  check("name").isLength({ min: 1 }),
  check("race").isLength({ min: 1 }),
  check("weight").isLength({ min: 1}),
  check("birthdate").isLength({ min: 8 }),
  check("type_pet").isLength({ min: 1}),
];

function validator(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  next();
}

module.exports = {
  validatorParams,
  validator,
};