const { check, validationResult } = require("express-validator");

validatorParams = [
    check("name_vet").isLength({ min: 1 }),
    check("description").isLength({ min: 1 }),
    check("date").isLength({ min: 1}),
    check("pet_id").isLength({ min: 1 }),
    ];
  
  function validator(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    next();
  }
  
  module.exports = {
    validatorParams,
    validator,
  };